Setup project.
You should clone this project via link from gitlab.


git clone https://gitlab.com/antonmazun/wa_09_07.git


In the project folder you should create virtual environment  "python -m venv env" (if your OS is MACOs , this command is  "python3 -m venv env")


Next step : activate env. On windows  - "cd env/Scripts " and run script "activate". On MACOS or Unix system  - source env/bin/activate.


In the folder where it is located file "req.txt" you should run command pip install -r req.txt


python manage.py migrate apply all migrations for django :)


python manage.py makemigrations  - create file of migrations


python manage.py migrate  - apply your changes in models.py to DB


python manage.py createsuperuser  - create admin user.


python manage.py runserver 9000 (optional you can changes port  , default port - 8000)


That`s all.