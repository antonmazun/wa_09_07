def add(a, b):
    return a + b


def sub(a, b):
    return a - b


def div(a, b):
    try:
        return a / b
    except ZeroDivisionError as err:
        return err


def mul(a, b):
    return a * b


calc_obj = {
    '+': add,
    '-': sub,
    '*': mul,
    '/': div
}
