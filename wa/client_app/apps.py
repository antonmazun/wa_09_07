from django.apps import AppConfig


class ClientAppConfig(AppConfig):
    name = 'client_app'

def count_char(str, char):
    str = str.lower()
    char = char.lower()
    return len([elem for elem in str if elem == char]) if char in str else 0


x = count_char('CCCabcd', 'c')  # > 4
print(x)
print(count_char('CcCabcd', 'A'))  # => 4
# count_char('CCCabcd', 'e')  # => ValueError
