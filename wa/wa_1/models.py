from django.db import models


# Create your models here.


class Category(models.Model):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class Book(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    desc = models.TextField(max_length=1000, blank=True, null=True)
    price = models.FloatField()
    is_sale = models.BooleanField(default=False)
    percent = models.FloatField(blank=True, null=True, default=4.5)
    year = models.DateField()
    rate = models.FloatField()
    file = models.FileField(upload_to='file_books/', blank=True, null=True)
    img = models.ImageField(upload_to='avatar_books/', blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.title, self.year)
