from django.contrib import admin
from .models import Category, Book


# Register your models here.

class BookAdmin(admin.ModelAdmin):
    list_display = ['title', 'price', 'is_sale', 'category', 'rate', 'percent']
    list_editable = ['is_sale', 'price', 'percent']
    list_filter = ['price', 'is_sale']


admin.site.register(Category)
admin.site.register(Book, BookAdmin)
