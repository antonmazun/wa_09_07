from django.urls import path, include
from . import views, auth_views

app_name = 'client'
urlpatterns = [
    path('', views.index, name='home'),
    path('about', views.about, name='about'),
    path('show-book/<int:pk>', views.book_info, name='book-info'),
    path('filters', views.filters, name='filters'),
    path('html', views.html_example),
    path('category-<int:pk>', views.filter_by_category),
    path('cabinet', views.cabinet, name='cabinet'),
    path('return-json', views.return_json),
    path('login', auth_views.login_, name='login'),
    path('register', auth_views.register_, name='register'),
    path('logout', auth_views.logout_, name='logout'),
    path('calculator' , views.calculator)
]
